p2


CLUSTERING

particional clustering: where data set is divided into clusters at the same level;
hierarchical clustering:: where clusters are also clustered in higher-level clusters;

Membership:
-exclusive, if each example belongs only to one cluster;
-overlapping if an example can belong to two or more clusters; 
-fuzzy clustering if all examples belong to all clusters with some continuous membership value between 0 and 1;
-probabilistic clustering if the membership value of each example to each cluster represents a probability.

K-MEANS:
simplificar o data set com um numero inferior de pontos, mas com a mesma "forma" mostrada no conjunto de dados original.
k-mean is an exclusive, partitional and prototyped-based clustering method.
It consists in dividing the data set into k clusters, each defined by the mean vector of the members if the cluster, wich is the prototype of that cluster.
