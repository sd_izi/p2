# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 18:41:57 2018

@author: Miguel Figueira & Diana Duarte
"""
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from sklearn.cluster import KMeans, DBSCAN
from sklearn.mixture import GaussianMixture
from sklearn.metrics import adjusted_rand_score, silhouette_score
from sklearn.neighbors import KNeighborsClassifier
from itertools import combinations


RADIUS = 6371
mpl.rcParams['legend.fontsize'] = 10

    
def plot_classes(labels,lon,lat, title, alpha=0.5, edge = 'k'):
    """Plot seismic events using Mollweide projection.
    Arguments are the cluster labels and the longitude and latitude
    vectors of the events"""
    img = mpimg.imread("Mollweide_projection_SW.jpg")        
    plt.figure(figsize=(10,5),frameon=False)    
    x = lon/180*np.pi
    y = lat/180*np.pi
    ax = plt.subplot(111, projection="mollweide")
    #print(ax.get_xlim(), ax.get_ylim())
    t = ax.transData.transform(np.vstack((x,y)).T)
    #print(np.min(np.vstack((x,y)).T,axis=0))
    #print(np.min(t,axis=0))
    clims = np.array([(-np.pi,0),(np.pi,0),(0,-np.pi/2),(0,np.pi/2)])
    lims = ax.transData.transform(clims)
    plt.close()
    plt.figure(figsize=(10,7),frameon=False)    
    plt.subplot(111)
    plt.imshow(img,zorder=0,extent=[lims[0,0],lims[1,0],lims[2,1],lims[3,1]],aspect=1)        
    plt.title(title)
    x = t[:,0]
    y= t[:,1]
    nots = np.zeros(len(labels)).astype(bool)
    diffs = np.unique(labels)    
    ix = 0   
    for lab in diffs[diffs>=0]:        
        mask = labels==lab
        nots = np.logical_or(nots,mask)        
        plt.plot(x[mask], y[mask],'o', markersize=4, mew=1,zorder=1,alpha=alpha, markeredgecolor=edge)
        ix = ix+1                    
    mask = np.logical_not(nots)    
    if np.sum(mask)>0:
        plt.plot(x[mask], y[mask], '.', markersize=1, mew=1,markerfacecolor='w', markeredgecolor=edge)
    plt.axis('off') 
    plt.show()
    plt.close()
    
#from lat and lon to x,y,z
def transform_coord(mat):
    fig= plt.figure(figsize=(10,10))
    ax=fig.gca(projection='3d')
    
    coords = np.zeros((mat.shape[0],3))
    aux = np.pi/180
    coords[:,0] = RADIUS * np.cos(mat[:,0] * aux) * np.cos(mat[:,1] * aux)
    coords[:,1] = RADIUS * np.cos(mat[:,0] * aux) * np.sin(mat[:,1] * aux)
    coords[:,2] = RADIUS * np.sin(mat[:,0] * aux)
    ax.plot(coords[:,0],coords[:,1],coords[:,2], 'o')
    plt.show()
    return coords

#Clustering Algorithm: K-Means
def k_means_alg(clusters, data):
    k = KMeans(clusters).fit(data)
    return k.predict(data)

#Clustering Algorithm: DBScan
def dbscan_alg(ε,data):
     db=DBSCAN(eps=ε,min_samples=4).fit_predict(data)
     return db
#Clustering Algorithm: Gaussian Mixture
def gaussian_mixture_alg(component, data):
    gm=GaussianMixture(component).fit(data)
    return gm.predict(data)
 #-----Params Selection-----
def sorted_kdist_graph(coords):
    same_class = np.zeros(len(coords))
    knc=KNeighborsClassifier(n_neighbors = 5).fit(coords,same_class )
    distances , x =knc.kneighbors(coords)
    plt.figure(figsize=(12,8))
    plt.plot(-np.sort(-distances[:,-1]))
    plt.show()
    plt.close()
def examine_alg(data, faults, labels,pairs):
    
    trueP, trueN, falseP, falseN = confusion_matrix(labels, faults,pairs)
    
    ##internal index: 
    #silhouette_score
    sil = silhouette_score(data, labels)
    
    ##external indexes:
    #adjusted Rand index
    adjust_rand = adjusted_rand_score(faults,labels)
    #Precision = true positives / (true positives + false positives)
    precision = trueP / (trueP + falseP)
    #Recall = true positives / (true positives + false negatives)
    recall = trueP / (trueP + falseN)
    #F1 measure
    f1_measure = 2*((precision*recall) / (precision + recall))
    #the Rand index
    rand_index = (trueP + trueN) / (trueP + trueN + falseP + falseN)
    
    return sil, adjust_rand, precision, recall, f1_measure, rand_index

def confusion_matrix(labels, faults,pairs):
    
    same_cluster = np.zeros(0)
    same_fault = np.zeros(0)
    
    same_cluster = np.append(same_cluster, labels[pairs[:,0]] == labels[pairs[:,1]])
    same_fault = np.append(same_fault, faults[pairs[:,0]] == faults[pairs[:,1]])
  
    trueP = 0
    trueN = 0
    falseP = 0
    falseN = 0
    for j in range(0,len(pairs)):
        if(same_cluster[j] == 1): # 1 - positives (same cluster, same fault)
            if(same_fault[j] == 1):
                trueP = trueP + 1
            else:
                falseP = falseP + 1
        else:
            if(same_fault[j] == 0): #0 - negatives (different cluster, different fault)
                trueN = trueN + 1
            else:
                falseN = falseN+ 1  
    return trueP, trueN, falseP, falseN
    

#DATA LOAD         
mat = np.loadtxt('tp2_data.csv',delimiter=',',skiprows=1,usecols=(2,3))
faults = np.loadtxt('tp2_data.csv',delimiter=',',skiprows=1,usecols=(-1))
coords = transform_coord(mat)
title = "Fault Lines"
plot_classes(faults, mat[:, 1], mat[:, 0], title)

#--------Algorithm's performance analysis-------
for i in range(0,3):
    index_sil = []
    index_adj = []
    index_precision = []
    index_recall = []
    index_f1_measure = []
    index_rand_index = []
    labels = []
    x_coords= []
    pairs=list(combinations(range(len(coords)),2))
    pairs = np.asarray(pairs)
    N=0
    if(i == 0):
        labels=k_means_alg
        n=2
        N=200
        increment= 2
        name="KMeans"
    elif( i== 1):
        labels =dbscan_alg
        n=10
        N=2300
        increment = 5
        name="DBScan"
    else:
        labels =gaussian_mixture_alg
        n=2
        N=200
        increment = 2
        name="GaussianMixture"
    print(name)
    for k in range (n,N,increment):
        lab=labels(k,coords)
        sil, adjust_rand,precision, recall, f1_measure, rand_index = examine_alg(coords,faults,lab,pairs)
        index_sil.append(sil)
        index_adj.append(adjust_rand)
        index_precision.append(precision)
        index_recall.append(recall)
        index_f1_measure.append(f1_measure)
        index_rand_index.append(rand_index)
        x_coords.append(k)
        percent =round((k-n)/ (N-n) *100)
        print('\rLoading: %d %%' % (percent), end = '\r')
        
    plt.figure(figsize=(12,8))
    plt.plot(x_coords,index_sil,'b',label="Silhouette score")
    plt.plot(x_coords,index_adj,'g',label="Rand index")
    plt.plot(x_coords,index_precision,'r', label= "Precision")
    plt.plot(x_coords,index_recall,'c', label ="Recall")
    plt.plot(x_coords,index_f1_measure,'m', label = "F1 measure")
    plt.plot(x_coords,index_rand_index,'y',label = "Adjusted Rand index")
    plt.legend(loc='upper right')
    plt.savefig(name+".png", dpi=300)
    plt.show()
    plt.close()
'''
print("----------------- K-Means -----------------")
# vary number of clusters - k_clusters




# after the best param selection
#plot best k
title = 'K-Means Clustering Algorithm'
plot_classes(km_labels, mat[:, 1], mat[:,0], title)
    

print("----------------- DBScan -----------------")
#DBScan
dbs_labels=[]

# vary neighbourhood distance ε
sorted_kdist_graph(coords)


# after the best param selection
min_pts=4
ε=150
dbs_labels=dbscan_alg(ε,min_pts,coords)
title ='DBScan Clustering Algorithm'
plot_classes(dbs_labels, mat[:,1], mat[:,0],title)

print("----------------- Gaussian Mixture -----------------")
#Gaussian Mixture Models
# vary number of gaussian components
gm_labels = []
component = 3

gm_labels=gaussian_mixture_alg(component, coords)
title = 'Gausian Mixture Algorithm'
plot_classes(gm_labels, mat[:,1], mat[:,0],title)
# after the best param selection

#plot best n_components

'''


